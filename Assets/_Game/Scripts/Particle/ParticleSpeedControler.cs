﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public struct SpeedParticleSetPack//struct for base sating on correct magnitude of car velocity
{
    [Tooltip("Speed When to start this section")]
    public float _rBSpeed;
    [Tooltip("Set Material On This Speed, if not set then default value")]
    public Material _material;
    [Tooltip("Speed of particle- set to minus value to default value")]
    public float startSpeed;
    [Tooltip("Lifetime of particle- set to minus value to default value")]
    public float lifetime;
    [Tooltip("MaxParticles of particle- set to minus value to default value")]
    public int maxParticles;
    [Tooltip("MaxParticles Emission of particle- set  _particleMaxParticles1 to minus value to default value")]
    public float emissionRate;
}
public class ParticleSpeedControler : MonoBehaviour//system to control particle giving speed effect-for now a lots of if for debugging and playtest
{
    [SerializeField] private VehicleManager _veMenager;
    private WheelDrive _car;
    private ParticleSystem _particle;
    private ParticleSystemRenderer _forSetMaterial;

    [Header("Stack of Settings for particle")]
    [SerializeField] private List<SpeedParticleSetPack> _stackOfSettings;
    [Header("Struct of deault value, if in stack there are minus value")]
    [Tooltip("Here _rbSpeed do nothing")]
    [SerializeField] private SpeedParticleSetPack _defaultValue;
    void Start()
    {
        _particle = this.GetComponent<ParticleSystem>();
        _forSetMaterial = this.GetComponent<ParticleSystemRenderer>();
        _particle.Stop();
        _stackOfSettings.Sort((x, y) => x._rBSpeed.CompareTo(y._rBSpeed));
    }


    // Update is called once per frame later change it to one time in final version
    [Obsolete]
    void FixedUpdate()
    {
        if(_car == null)
        {

            _car = _veMenager.SelectedCar;

        }
        else
        {
            
            for (int i = _stackOfSettings.Count - 1; i >= 0; i--)
            {
                if(_stackOfSettings[i]._rBSpeed> _car.ActualSpeed && i!=0)//if speed not reached dismiss this setting pack
                {
                    continue;
                }else if(_stackOfSettings[i]._rBSpeed <= _car.ActualSpeed)//use this seeeting pack if speed accurate
                {
                    if (!_particle.isPlaying) _particle.Play();
                    _forSetMaterial.material = _stackOfSettings[i]._material != null ?  _stackOfSettings[i]._material : _defaultValue._material;
                    _particle.startSpeed = _stackOfSettings[i].startSpeed >=0? _stackOfSettings[i].startSpeed : _defaultValue.startSpeed;
                    _particle.startLifetime = _stackOfSettings[i].lifetime >= 0 ? _stackOfSettings[i].lifetime : _defaultValue.lifetime;
                    _particle.maxParticles = _stackOfSettings[i].maxParticles >= 0 ? _stackOfSettings[i].maxParticles : _defaultValue.maxParticles;
                    _particle.emissionRate = _stackOfSettings[i].emissionRate >= 0 ? _stackOfSettings[i].emissionRate : _defaultValue.emissionRate;
                    break;
                }
                else//if too slow deactivate
                {
                    _particle.Stop();
                }
            }
        }
    }
}
