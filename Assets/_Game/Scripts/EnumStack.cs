﻿using System;
public class EnumStack
{
	//wchich wheels need to had added tourque by car
	[Serializable]
	public enum DriveType
	{
		RearWheelDrive,
		FrontWheelDrive,
		AllWheelDrive
	}
	//for input system names
	public enum Controls
	{
		Horizontal, Vertical, HandBar
	}
	//names of wheels 0-first 1-last, l-left r-right
	public enum WheelNames
	{
		a0l, a0r, a1l, a1r
	}
	//name of used playerprefs keys
	public enum PlayerPrefsKeys
    {
		decidedCar, numberOfAi,numberOfLoops,ignoreCollision
	}
	//name of used playerprefs keys for audio
	public enum NameOfPlayerPref
	{
		masterVolume, musicVolume, soundEffectVolume
	}
}
