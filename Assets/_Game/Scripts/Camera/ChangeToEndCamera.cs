﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class ChangeToEndCamera : MonoBehaviour
{
    // Start is called before the first frame update
    private CinemachineVirtualCamera _endCamera;
    private int _newPriority = 11;
    void Awake()
    {
        AIVone.OnGameEnd += ChangeCamera;
        _endCamera = this.GetComponent<CinemachineVirtualCamera>();
    }

    // on game end change camera to virtual end camera (in iddle of circle)
    private void ChangeCamera(AIVone ai)
    {
        _endCamera.Priority = _newPriority;
    }
}
