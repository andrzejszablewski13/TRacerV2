using UnityEngine;
using Cinemachine;

//set following for virtual camera
public class DriftCamera : MonoBehaviour
{
    //camera move-believe dont need explenation
    [SerializeField] private float _smoothing = 6f;
    [SerializeField] private CinemachineVirtualCamera _camera,_endCamera;
    private Transform _lookAtTarget;//on player car
    private Transform _positionTarget;//starting camera position
    private bool setActive = false;

    public Transform PositionTarget { get => _positionTarget; set => _positionTarget = value; }
    public Transform LookAtTarget { get => _lookAtTarget; set => _lookAtTarget = value; }
    //if set true will active camera follow
    public bool SetActive { get => setActive; set { setActive = value; if (value) { SetCamera(); } } } 

    //set value to follow selected camera by cinemachine virtual camera
    private void SetCamera()
    {
        _camera.Follow = PositionTarget;
        _camera.LookAt = LookAtTarget;
        _endCamera.LookAt = LookAtTarget;
    }
}
