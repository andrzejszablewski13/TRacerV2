﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionBetwwenCars : MonoBehaviour
{
    [SerializeField] private int _layerNumber;
    [SerializeField] private bool _setOnAwake = false;

    //if set awake decide if collision between cars schould appper (by value in playerpref ignoreCollision)
    private void Awake()
    {
        if (_setOnAwake && PlayerPrefs.HasKey(EnumStack.PlayerPrefsKeys.ignoreCollision.ToString()))
        {
            Physics.IgnoreLayerCollision(_layerNumber, _layerNumber, PlayerPrefs.GetInt(EnumStack.PlayerPrefsKeys.ignoreCollision.ToString())==1);
        }
    }
    //change playerpref ignoreCollision to true=1 or false 0
    public void SetColision(bool isTrue)
    {
        Physics.IgnoreLayerCollision(_layerNumber, _layerNumber, !isTrue);
        PlayerPrefs.SetInt(EnumStack.PlayerPrefsKeys.ignoreCollision.ToString(), !isTrue ? 1 : 0);
    }
}
