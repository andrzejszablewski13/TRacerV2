﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class VehicleDecideButtons : MonoBehaviour//set car that will be played
{
    [SerializeField] private Button[] _buttons;
    [SerializeField]
    private Button _goback;
    [SerializeField] private GameObject  _startMenu;
    private int _indexOfNextScene = 1;
    void Start()
    {
        for (int i = 0; i < _buttons.Length; i++)//set buttons to decide car
        {
            int number=i;
            _buttons[i].onClick.AddListener(() => SetCar(number));
        }
        _goback.onClick.AddListener(()=>MainButtons.ChangeMenu(_startMenu,this.gameObject));//set button to return to previus view
    }
    private void SetCar(int i)//if car selected save its number to player prefs decidedCar and go to game scene
    {
        PlayerPrefs.SetInt(EnumStack.PlayerPrefsKeys.decidedCar.ToString(), i);
        SceneManager.LoadScene(_indexOfNextScene);
    }

}
