﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//upgrade version of SelectNumberOf that do something when given value is reached or overpassed (and undode if is below)
//now is deactivating collision between cars and option to activate collision between cars
public class SetNumberPlus : SelectNumberOf
{
    [SerializeField] private int _blockFrom;
    [SerializeField] private Toggle _toggle;
    [SerializeField] private CollisionBetwwenCars _script;
    protected override void LowerNumber()
    {
        base.LowerNumber();
        if (base._temp < _blockFrom)
        {
            _script.SetColision(true);
            
            _toggle.enabled = true;
        }
    }
    protected override void GreaterNumber()
    {
        base.GreaterNumber();
        if (base._temp >= _blockFrom)
        {
            _script.SetColision(false);
            _toggle.isOn = false;
            _toggle.enabled = false;
        }
    }
}
