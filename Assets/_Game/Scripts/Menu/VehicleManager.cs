﻿using UnityEngine;

public class VehicleManager : MonoBehaviour//select car decided by player and set camera on it
{
    [SerializeField] private GameObject[] _vehicle;
    [SerializeField] private DriftCamera _camera;

    private Transform _temp;
    private readonly int _camRigIndex = 5;
    private readonly int _camLookAtTargetIndex = 0;
    private readonly int _camPositionIndex = 1;
    private WheelDrive _selectedCar;
    private int _tempPrefSave;
    private Env _environment;
    public WheelDrive SelectedCar { get => _selectedCar;private set => _selectedCar = value; }
    //read sets from playerprefs and active proper card as player
    private void Awake()
    {
        _environment = this.GetComponentInParent<Env>();
        for (int i = 0; i < _vehicle.Length; i++)//deactivate all cars for start
        {
            _vehicle[i].SetActive(false);
        }
        try //try read car number selected by player else give him first car
        {
            _tempPrefSave = PlayerPrefs.GetInt(EnumStack.PlayerPrefsKeys.decidedCar.ToString());
            _vehicle[_tempPrefSave].SetActive(true);
            _temp = _vehicle[_tempPrefSave].transform.GetChild(_camRigIndex);
            SelectedCar = _vehicle[_tempPrefSave].GetComponent<WheelDrive>();
            
        }
        catch
        {

            _vehicle[0].SetActive(true);
            _temp = _vehicle[0].transform.GetChild(_camRigIndex);
            SelectedCar = _vehicle[0].GetComponent<WheelDrive>();
        }
        if (!_environment.UsedCars.Contains(SelectedCar.GetComponent<AIVone>()))//add sel car to env as taking part in the race
        {
            _environment.UsedCars.Add(SelectedCar.GetComponent<AIVone>());
        }
        //set camera to follow sel car (by another script)
            _camera.LookAtTarget = _temp.GetChild(_camLookAtTargetIndex);
        _camera.PositionTarget = _temp.GetChild(_camPositionIndex);
        _camera.SetActive = true;
    }
}
