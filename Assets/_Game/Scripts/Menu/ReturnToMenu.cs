﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class ReturnToMenu : MonoBehaviour//load other scene, default menu scene
{
    [SerializeField] private int _sceneIndex=0;

    public void LoadMenuScene()
    {
        SceneManager.LoadScene(_sceneIndex);
    }
}
