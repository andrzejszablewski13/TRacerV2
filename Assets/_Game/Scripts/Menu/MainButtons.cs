﻿using UnityEngine;
using UnityEngine.UI;

public class MainButtons : MonoBehaviour//script to control buttons in main view of main scene
{
    [SerializeField] private Button _playButton;
    [SerializeField] private Button _exitButton;
    [SerializeField] private Button _settingButton, _creditsButton;
    [SerializeField] private GameObject _backGround,_decideCar,_canvas,_settings,_credits;

   private void Awake()
    {
        _playButton.onClick.AddListener(Play);
        _exitButton.onClick.AddListener(Exit);
        _settingButton.onClick.AddListener(()=> { ChangeMenu(_settings,this.gameObject); });
        _creditsButton.onClick.AddListener(() => { ChangeMenu(_credits, this.gameObject); });

    }
    public static void ChangeMenu(GameObject target,GameObject here)
    {
        target.SetActive(true);
        here.SetActive(false);
    }
    public void Play()
    {
        _decideCar.SetActive(true); 
        this.gameObject.SetActive(false);
    }
    private void Exit()
    {
        Application.Quit();
    }
}
