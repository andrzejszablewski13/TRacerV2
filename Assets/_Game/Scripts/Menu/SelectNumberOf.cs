﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

//select number of something by clicking left button(-1), right button(+1) and min/max values and saving it to player prefs
public class SelectNumberOf : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] protected EnumStack.PlayerPrefsKeys _key;
    [SerializeField] protected int _min, _max,_default=3;
    [SerializeField] protected Button _left, _right;
    [SerializeField] protected TextMeshProUGUI _numberText;
    protected int _temp;
    protected void Start()
    {
        PlayerPrefs.SetInt(_key.ToString(), _default);
        _left.onClick.RemoveAllListeners();
        _right.onClick.RemoveAllListeners();
        _left.onClick.AddListener(LowerNumber);
        _right.onClick.AddListener(GreaterNumber);
        _numberText.text = _default.ToString();
    }

    protected virtual void LowerNumber()
    {
        _temp = PlayerPrefs.GetInt(_key.ToString());
        if (_temp > _min)
        {
            _temp--;
            PlayerPrefs.SetInt(_key.ToString(), _temp);
            _numberText.text = _temp.ToString();
            if (_temp == _min)
            {
                _left.enabled = false;

            }
            if (_temp < _max)
            {
                _right.enabled = true;
            }
        }
    }
    protected virtual void GreaterNumber()
    {
        _temp = PlayerPrefs.GetInt(_key.ToString());
        if (_temp < _max)
        {
            _temp++;
            PlayerPrefs.SetInt(_key.ToString(), _temp);
            _numberText.text = _temp.ToString();
            if (_temp==_max)
            {
                _right.enabled = false;
            }
            if (_temp > _min)
            {
                _left.enabled = true;
            }
        }
    }
}
