﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//comparator to compare pos of cars in race depend on pos (first on reached checkpoint desc, second by distance to next checkpoint asc)
public class ICompareRacers : IComparer<AIVone>
{
    private int _checkPointAdv;
    public int Compare(AIVone x, AIVone y)
    {
        _checkPointAdv = y.ReachedChePoints.CompareTo(x.ReachedChePoints);
        if(_checkPointAdv==0)
        {
            return x.DistanceToCheckPoint().CompareTo(y.DistanceToCheckPoint());
        }else
        {
            return _checkPointAdv;
        }
    }
}
