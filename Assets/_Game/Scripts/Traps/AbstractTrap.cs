﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//abstarct for traps
public class AbstractTrap : MonoBehaviour
{
    //list of affected cars
    protected List<WheelDrive> _carsEffected;
    //time of effect
    [SerializeField] protected float _effectTime=5;
    private void Awake()
    {
        _carsEffected = new List<WheelDrive>();
    }
    //do effect on trigger enter and unit to list to not stack effects from one trap
    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent(out WheelDrive _driver) && !_carsEffected.Contains(_driver))
        {
            StartCoroutine(DoEffect(_driver));
            _carsEffected.Add(_driver);
        }
    }
    //ovveride to do effect, add it before base too active and remove after
    public virtual IEnumerator DoEffect(WheelDrive _driver)
    {

        yield return new WaitForSeconds(_effectTime);
        _carsEffected.Remove(_driver);
    }
}
