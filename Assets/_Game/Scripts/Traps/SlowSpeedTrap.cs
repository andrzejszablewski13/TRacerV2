﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//slow or speed up car
public class SlowSpeedTrap : AbstractTrap
{
    //bonus force value
    [SerializeField] private float _numericalStrenght;
    //add bonus possible force for car 'engine' for limited time
    public override IEnumerator DoEffect(WheelDrive _driver)
    {
        _driver._maxTorque += _numericalStrenght;
        yield return base.DoEffect(_driver);
        _driver._maxTorque -= _numericalStrenght;
    }
}
