﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//trap that frezze car
public class FreezeTrap : AbstractTrap
{
    [Header("options ver of frezzzing movement")]
    //active witch version of freezing movement will be use (can many at the same time)
    [SerializeField] private bool _ver1=false, _ver2=false,_ver3=false,_destroyAfter=false;
    public override IEnumerator DoEffect(WheelDrive _driver)
    {
        float startTourque;
        startTourque=_driver._maxTorque ;
        //reset torque (something like vel on wheels)
        if (_ver1)
        {
            _driver._maxTorque = 0;
        }
        //reset vel and ang vel
        if(_ver2)
        {
            _driver.GetComponent<Rigidbody>().velocity = Vector3.zero;
            _driver.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
        }
        //block any other source of force for moment of effect work
        if (_ver3)
        {
            _driver.GetComponent<Rigidbody>().isKinematic = true;
        }
        //made bomb work once
        if(_destroyAfter)
        {
            this.GetComponent<BoxCollider>().enabled = false;
            this.GetComponent<MeshRenderer>().enabled = false;
        }
        yield return new WaitForSeconds(_effectTime);
        _carsEffected.Remove(_driver);
        //return values from begining (witchout vel)
        if (_ver1)
        {
            _driver._maxTorque = startTourque;
        }
        if (_ver3)
        {
            _driver.GetComponent<Rigidbody>().isKinematic = false;
        }
    }
}
