﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoBack : MonoBehaviour//ga back in view on canvas
{
    [SerializeField] private GameObject _mainMenu;
    [SerializeField] private Button _button;
    void Start()
    {
        _button.onClick.AddListener(()=>MainButtons.ChangeMenu(_mainMenu, this.gameObject));
    }    
}
