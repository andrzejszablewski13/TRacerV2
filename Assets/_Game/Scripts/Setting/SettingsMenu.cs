﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using static EnumStack;

//settings of audio
public class SettingsMenu : MonoBehaviour
{
    [SerializeField] private Slider _masterSoundV, _musicV, _soundEffectV;
    [SerializeField] private AudioVolumeSyn _musicInMenu;//actualize music in menu volume in realtime
    
    void Awake()//read actual values and set slithers to them, else set to 0, also connect methods witch slithers
    {
        
        if (!PlayerPrefs.HasKey(NameOfPlayerPref.masterVolume.ToString()))
        {
            PlayerPrefs.SetFloat(NameOfPlayerPref.masterVolume.ToString(), _masterSoundV.value);
            PlayerPrefs.SetFloat(NameOfPlayerPref.musicVolume.ToString(), _musicV.value);
            PlayerPrefs.SetFloat(NameOfPlayerPref.soundEffectVolume.ToString(), _soundEffectV.value);
        }else
        {
            _masterSoundV.value = PlayerPrefs.GetFloat(NameOfPlayerPref.masterVolume.ToString());
            _musicV.value = PlayerPrefs.GetFloat(NameOfPlayerPref.musicVolume.ToString());
            _soundEffectV.value = PlayerPrefs.GetFloat(NameOfPlayerPref.soundEffectVolume.ToString());
        }
        _masterSoundV.onValueChanged.AddListener(SetMasterAudio);
        _musicV.onValueChanged.AddListener(SetMusic);
        _soundEffectV.onValueChanged.AddListener(SetAudioEffect);
    }
    //methods to use sliders
    private void SetMasterAudio(float i)
    {
        PlayerPrefs.SetFloat(NameOfPlayerPref.masterVolume.ToString(), i);
        _musicInMenu.SetVolume();
    }
    private void SetMusic(float i)
    {
        PlayerPrefs.SetFloat(NameOfPlayerPref.musicVolume.ToString(), i);
        _musicInMenu.SetVolume();
    }
    private void SetAudioEffect(float i)
    {
        PlayerPrefs.SetFloat(NameOfPlayerPref.soundEffectVolume.ToString(), i);
    }
}
