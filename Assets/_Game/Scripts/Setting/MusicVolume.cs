﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicVolume : AudioVolumeSyn//set volume based of it type, default is music
{
    [SerializeField] private EnumStack.NameOfPlayerPref _musicType;
    public override void Start()
    {
        _typeOfAudio = _musicType;
        base.Start();
    }
    public override void SetVolume()
    {
        base.SetVolume();

        audioSource.volume *= PlayerPrefs.GetFloat(_typeOfAudio.ToString());
        
    }
}
