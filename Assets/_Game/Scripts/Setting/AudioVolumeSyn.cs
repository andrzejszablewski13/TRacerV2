﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public abstract class AudioVolumeSyn : MonoBehaviour, IVoluming//base for setting volume of audio source
{
    // Start is called before the first frame update
    protected EnumStack.NameOfPlayerPref _typeOfAudio;
    [SerializeField][Range(0,1)] private float _localVolume=1;//local volume modyficator
    protected AudioSource audioSource;
    public virtual void Start()
    {
        audioSource = this.GetComponent<AudioSource>();
        SetVolume();
    }
    public virtual void SetVolume()
    {
        audioSource.volume = PlayerPrefs.GetFloat(EnumStack.NameOfPlayerPref.masterVolume.ToString()) * _localVolume;
    }

}
