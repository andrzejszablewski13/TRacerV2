﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//decide how many ai should take in race
public class ActiveAis : MonoBehaviour
{
    private List<AIVone> _aiCars;
    private int _activeAiNumber;
    private int _tempAiActive;
    public List<AIVone> ActivateAI()
    {
        _tempAiActive = 0;
        _aiCars = new List<AIVone>();
        if(PlayerPrefs.HasKey(EnumStack.PlayerPrefsKeys.numberOfAi.ToString()))//take number of active ai from playerpref numberOfAi
        {
            _activeAiNumber = PlayerPrefs.GetInt(EnumStack.PlayerPrefsKeys.numberOfAi.ToString());
            if(_activeAiNumber>this.transform.childCount)
            {
                _activeAiNumber = this.transform.childCount;
            }
        }else //else take all
        {
            _activeAiNumber = this.transform.childCount;
        }
        foreach (AIVone item in this.GetComponentsInChildren<AIVone>())//select correct number of ai cars
        {
            _tempAiActive++;
            if(_tempAiActive<= _activeAiNumber)
            {
                _aiCars.Add(item);
                item.gameObject.SetActive(true);
            }else
            {
                item.gameObject.SetActive(false);
            }
            
        }
        return _aiCars;//return ai cars that will take part in race
    }

}
