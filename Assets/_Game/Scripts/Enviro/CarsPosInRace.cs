﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//calculate pos of cars in race, sort list of racers depend on their pos and refresch bonus that depend on cars pos
public class CarsPosInRace : MonoBehaviour
{
    private Env _env;
    public delegate void PosRefresch();
    public static PosRefresch OnPosRefresch;
    private ICompareRacers _compareRecars;
    [SerializeField] private float _refreschTime = 0.1f;
    void Awake()
    {
        _env = this.GetComponent<Env>();
        _compareRecars = new ICompareRacers();
        StartCoroutine(RefreschInTime());
    }

    public void RefreschBonus()
    {
        RefreschPos();
        for (int i = 0; i < _env.UsedCars.Count; i++)
        {
            if (i == 0)
            {
                _env.UsedCars[i].WheelDrive.SetBonus();
            }
            else
            {
                _env.UsedCars[i].WheelDrive.SetBonus(_env.UsedCars[0].ReachedChePoints - _env.UsedCars[i].ReachedChePoints);
                
            }
        }
    }
    public void RefreschPos()
    {
        _env.UsedCars.Sort(_compareRecars);
        OnPosRefresch?.Invoke();
    }


    private IEnumerator RefreschInTime()
    {
        while(true)
        {
            yield return new WaitForSeconds(_refreschTime);
            RefreschBonus();
        }
    }
}
