﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//show player pos in race
public class PlayerPos : AbstractPlayerShowData
{

    [SerializeField] private string _startText = "Pos: ";

    protected override IEnumerator UpdateText()
    {
        while (true)
        {
            _text.text = _startText + (_environment.UsedCars.IndexOf(_car) + 1).ToString() + _divider + _environment.UsedCars.Count;
            yield return new WaitForSeconds(_timeToUpdate);
        }
    }


}
