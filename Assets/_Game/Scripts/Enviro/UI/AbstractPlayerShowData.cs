﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

//abstart for data showed on game UI
public abstract class AbstractPlayerShowData : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] protected VehicleManager _veMenager;
    protected AIVone _car;
    protected TextMeshProUGUI _text;
    [SerializeField] protected Env _environment;
    [SerializeField] protected float _timeToUpdate = 1;
    protected readonly string _divider = "/";
    protected IEnumerator _corutin;
    void Start()
    {
        _text = this.GetComponent<TextMeshProUGUI>();
        _car = _veMenager.SelectedCar.GetComponent<AIVone>();
        _corutin = UpdateText();
        StartCoroutine(_corutin);
    }
    protected abstract IEnumerator UpdateText();

}
