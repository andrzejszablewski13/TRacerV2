﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


//show text on end showing witch pos you take
public class PlayerEndPos : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] private Env _environment;
    [SerializeField] private readonly string _startText = "Ended as: ";
    [SerializeField] private readonly string[] _textNumbers = { "First", "Second", "Third","Thourght","Fifth","Sixth","Seventh","Eith","Ninth" };
    private TextMeshProUGUI _text;
    private void Awake()
    {
        _text = this.GetComponent<TextMeshProUGUI>();
        AIVone.OnGameEnd += ShowText;
    }
    // Update is called once per frame
    private void ShowText(AIVone _car)
    {
        _text.text = _startText + _textNumbers[_environment.UsedCars.IndexOf(_car)];
        _text.enabled=true;
    }

}
