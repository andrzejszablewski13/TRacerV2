﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//show number of loops that had been made compared to max number of loops, if reached max end it
public class PlayerLoop : AbstractPlayerShowData
{
    [SerializeField] private string _startText = "LoopNr: ";
    protected override IEnumerator UpdateText()
    {
        while (true)
        {
            if (_car.LoopNr < _environment.LoopsMaxNumber)
            {
                _text.text = _startText + (_car.LoopNr + 1).ToString() + _divider + _environment.LoopsMaxNumber;
            }
            else
            {
                StopCoroutine(_corutin);
            }
            yield return new WaitForSeconds(_timeToUpdate);
        }
    }
}
