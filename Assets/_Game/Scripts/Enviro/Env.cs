﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//enviroment controller
public class Env : MonoBehaviour
{
    public GameObject checkPoints;
    public List<AIVone> UsedCars;
    private List<MeshCollider> _checkpointList;
    private List<SpriteRenderer> _respawnsPointsList;
    private int _loopsMaxNumber = 3;
    [SerializeField] private ActiveAis _activateAI;
    
    private void Awake()
    {
        //add active ais to list of cars in race
        UsedCars.AddRange(_activateAI.ActivateAI());
        //set number of loops if set
        if (PlayerPrefs.HasKey(EnumStack.PlayerPrefsKeys.numberOfLoops.ToString()))
        {
            _loopsMaxNumber = PlayerPrefs.GetInt(EnumStack.PlayerPrefsKeys.numberOfLoops.ToString());
        }
        //set checkpoints and respawn points pos
        _checkpointList = new List<MeshCollider>(checkPoints.GetComponentsInChildren<MeshCollider>());
        _respawnsPointsList = new List<SpriteRenderer>(checkPoints.GetComponentsInChildren<SpriteRenderer>());
    }
    //get only read access to some elements
    public List<MeshCollider> CheckpointList { 
        get {
            if(_checkpointList==null)
            {
                _checkpointList = new List<MeshCollider>(checkPoints.GetComponentsInChildren<MeshCollider>());
            }
            return _checkpointList;
        }
        
    }
    public List<SpriteRenderer> RespawnsPointsList
    {
        get
        {
            if (_respawnsPointsList == null)
            {
                _respawnsPointsList = new List<SpriteRenderer>(checkPoints.GetComponentsInChildren<SpriteRenderer>());
                
            }
            return _respawnsPointsList;
        }

    }

    public int LoopsMaxNumber { get => _loopsMaxNumber;}
}
