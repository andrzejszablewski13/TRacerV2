﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxRotate : MonoBehaviour
{
    [SerializeField] private float _rotationMultiplier = 3;
    private readonly string _shaderKeyRotation = "_Rotation";
     void Update()
    {
        //Sets the float value of "_Rotation", adjust it by Time.time and a multiplier.
        RenderSettings.skybox.SetFloat(_shaderKeyRotation, Time.time * _rotationMultiplier);
    }
}