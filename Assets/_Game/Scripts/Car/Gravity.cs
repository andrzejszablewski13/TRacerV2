﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour//artificial gravity that work towards wall of the map
{
    
    private Rigidbody _rb;
    [SerializeField] private float _gravityForce = 9.14f;
    void Start()
    {
        _rb = this.GetComponent<Rigidbody>();
    }


    void FixedUpdate()//gravity - in precisize it work oo local down direction
    {
        _rb.AddForce(-this.transform.up * _gravityForce * _rb.mass);
    }
}
