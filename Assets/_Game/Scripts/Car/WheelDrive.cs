﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;


//script to control car (change input/ai data to wheels settings), partially not our code
public abstract class WheelDrive : MonoBehaviour
{

    [Tooltip("Maximum steering angle of the wheels")]
    [SerializeField] protected float _maxAngle = 45f;
    [Tooltip("Maximum torque applied to the driving wheels")]
    [SerializeField] public float _maxTorque = 3000f;
	[Tooltip("Maximum brake torque applied to the driving wheels")]
    [SerializeField] protected float _brakeTorque = 300000f;
	[Tooltip("If you need the visual wheels to be attached automatically, drag the wheel shape here.")]
    [SerializeField] protected GameObject _wheelShape,_backWheelShape;

    [Tooltip("The vehicle's speed when the physics engine can use different amount of sub-steps (in m/s).")]
    [SerializeField] protected float _criticalSpeed = 5f;
	[Tooltip("Simulation sub-steps when the speed is above critical.")]
    [SerializeField] protected int _stepsBelow = 5;
	[Tooltip("Simulation sub-steps when the speed is below critical.")]
    [SerializeField] protected int _stepsAbove = 1;

	[Tooltip("The vehicle's drive type: rear-wheels drive, front-wheels drive or all-wheels drive.")]
	[SerializeField] protected EnumStack.DriveType _driveType= EnumStack.DriveType.AllWheelDrive;
    [Header("Bonuses from pos")]
    [Tooltip("TorqueUpForDistance")]
    [SerializeField] private float _bonusPerChPoint = 100;
    [Tooltip("multiplayer bonus torgue to instant bonus force")]
    [SerializeField] private float _bonusForceMP = 0.01f;
    [Tooltip("bonus value of multiplayer for cars behind")]
    [SerializeField] private float _baseMPValue = 0.5f;

    protected WheelCollider[] _mWheels;
    protected float _angle, _torque, _handBrake;
    protected Quaternion _q;
    protected Vector3 _p;
    protected float _tempInputLocation;
    protected Rigidbody _rb;
    private float _actualSpeed;
    private readonly Quaternion _rotateToOtherYAxe = Quaternion.Euler(0, 180, 0);
    public float ActualSpeed { get => _actualSpeed;}


    // Find all the WheelColliders down in the hierarchy.
    void Start()
	{
        BeforeScriptStart();
    }


    protected void BeforeScriptStart()
    {
        _rb = this.GetComponent<Rigidbody>();
        _mWheels = this.GetComponentsInChildren<WheelCollider>();//find place for wheel

        for (int i = 0; i < _mWheels.Length; ++i)//add wheels
        {
            WheelCollider wheel = _mWheels[i];

            // Create wheel shapes only when needed.
            if(_backWheelShape!=null &&
                (_mWheels[i].gameObject.name== EnumStack.WheelNames.a1l.ToString() || _mWheels[i].gameObject.name == EnumStack.WheelNames.a1r.ToString()))
            {
                GameObject ws = Instantiate(_backWheelShape);
                ws.transform.parent = wheel.transform;
            }
            else
            if (_wheelShape != null)
            {
                GameObject ws = Instantiate(_wheelShape);
                ws.transform.parent = wheel.transform;
            }
        }
 
    }



    void Update()
	{
        MovingSystem();
    }
    protected virtual void MovingSystem()//control system for car-not our code
    {
        _actualSpeed= _rb.velocity.magnitude;

        MovingSystemCore();
    }
    protected void MovingSystemCore()
    {
        foreach (WheelCollider wheel in _mWheels)
        {
            // A simple car where front wheels steer while rear ones drive.
            if (wheel.transform.localPosition.z > 0)
                wheel.steerAngle = _angle;

            if (wheel.transform.localPosition.z < 0)
            {
                wheel.brakeTorque = _handBrake;
            }

            if (wheel.transform.localPosition.z < 0 && _driveType != EnumStack.DriveType.FrontWheelDrive)
            {
                wheel.motorTorque = _torque;
            }

            if (wheel.transform.localPosition.z >= 0 && _driveType != EnumStack.DriveType.RearWheelDrive)
            {
                wheel.motorTorque = _torque;
            }

            
            // Update visual wheels if any.
            if (_wheelShape)
            {
                wheel.GetWorldPose(out _p, out _q);

                // Assume that the only child of the wheelcollider is the wheel shape.
                Transform shapeTransform = wheel.transform.GetChild(0);

                if (wheel.name == EnumStack.WheelNames.a0l.ToString() || wheel.name == EnumStack.WheelNames.a1l.ToString())
                {
                    shapeTransform.rotation = _q * _rotateToOtherYAxe;
                    shapeTransform.position = _p;
                }
                else
                {
                    shapeTransform.position = _p;
                    shapeTransform.rotation = _q;
                }
            }
        }
    }
    protected abstract void ControlsSystem();//player controls-our code

    private float _actualBonusValue;
    public void SetBonus( float multiplayer = 0)//set bonus depend on car pos in race
    {

        _maxTorque -= _actualBonusValue;
        multiplayer += _baseMPValue;
        _actualBonusValue = multiplayer * _bonusPerChPoint;
        this.GetComponent<Rigidbody>().AddRelativeForce(Vector3.forward * _actualBonusValue * _bonusForceMP, ForceMode.Impulse);
        _maxTorque += _actualBonusValue;

    }

}
