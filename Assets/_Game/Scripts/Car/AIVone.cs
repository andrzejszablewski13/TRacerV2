﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Sensors;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Policies;

//ai for car witch option of manual control
[RequireComponent(typeof(WheelDriveForAI))]
[RequireComponent(typeof(Unity.MLAgents.Policies.BehaviorParameters))]
[RequireComponent(typeof(DecisionRequester))]
public class AIVone : Agent
{

    private Vector3 _startPoint;
    private Quaternion _startRotation;
    private float _vertical=0,_horizontal=0;
    private bool _reachedCheckPoint;
    private Rigidbody _rB;
    private WheelDriveForAI _wheelDrive;
    private Env _environment;
    private Queue<MeshCollider> _queneOfCheckPoints;
    private Queue<SpriteRenderer> _respawnPoints;
    private string _lastCheckPoint;
    private int _loopNr = 0, _reachedChePoints=0;
    private Vector3 _closestPointToCheckPoint;
    [Header("How fast wheel cahnge degree per frame")]
    [SerializeField] private float _wheelChangeRate = 0.03f;
    [Header("Req Dis to checkpoint")]
    [SerializeField] private float _distanceToEnd = 1f;
    //public int carTypeId = 0;
    [Header("ForDebug")]
    public float ver, hor;
    public int ReachedChePoints { get => _reachedChePoints; }
    public WheelDriveForAI WheelDrive { get => _wheelDrive; }
    public int LoopNr { get => _loopNr;}
    public delegate void GameEnd(AIVone ai);
    public static GameEnd OnGameEnd;
    private BehaviorParameters _behParametrs;
    private float _rewardForTime= -0.002f, _rewardFOrEnd= 0.8f, _rewardForFirst= 0.2f;

    //initialize===Awake
    public override void Initialize()
    {
        base.Initialize();
        _startPoint = this.transform.position;
        _startRotation = this.transform.rotation;
        _rB = this.GetComponent<Rigidbody>();
        _wheelDrive = this.GetComponent<WheelDriveForAI>();
        _environment = this.GetComponentInParent<Env>();
        _queneOfCheckPoints = new Queue<MeshCollider>(_environment.CheckpointList);
        _respawnPoints = new Queue<SpriteRenderer>(_environment.RespawnsPointsList);
        _lastCheckPoint = _environment.CheckpointList[_environment.CheckpointList.Count - 1].name;
        _reachedCheckPoint = false;
        _behParametrs = this.GetComponent<BehaviorParameters>();
    }
    //begin on begin of each episod (on succesful or failiad reaching of checkpoint)
    public override void OnEpisodeBegin()
    {
        base.OnEpisodeBegin();
        if (_queneOfCheckPoints==null)//if emty full list of checkpoints and respawn points
        {
            //in case off no quene create it
            _queneOfCheckPoints = new Queue<MeshCollider>(_environment.CheckpointList);
            _respawnPoints = new Queue<SpriteRenderer>(_environment.RespawnsPointsList);
        }
        if(!_reachedCheckPoint)//if fail respawn
        {
            this.transform.position = _startPoint;
            this.transform.rotation = _startRotation;
            _wheelDrive.Horizontal = _horizontal;
            _wheelDrive.Vertical = _vertical;
            _wheelDrive.HandBar = false;
        }
        else //else continue
        {
            
            _queneOfCheckPoints.Enqueue(_queneOfCheckPoints.Dequeue());
            _startPoint = _respawnPoints.Peek().transform.position;
            _startRotation = _respawnPoints.Peek().transform.rotation;
            _respawnPoints.Enqueue(_respawnPoints.Dequeue());
            _reachedCheckPoint = false;
            
        }
        if(_queneOfCheckPoints.Peek().name== _lastCheckPoint && _behParametrs.BehaviorType==BehaviorType.HeuristicOnly)//if controlled by player and reached last cheackpoint
        {
            //increase nummber of loops
                _loopNr++;
            if (LoopNr == _environment.LoopsMaxNumber)//if reached max loop number start end game and made car be controlled by ai from now
            {
                OnGameEnd(this);
                _behParametrs.BehaviorType = BehaviorType.InferenceOnly;
            }
        }



    }
    public override void CollectObservations(VectorSensor sensor)//collect all data and send to ai to decide
    {
        base.CollectObservations(sensor);
        _closestPointToCheckPoint = _queneOfCheckPoints.Peek().ClosestPoint(this.transform.position);
        sensor.AddObservation(this.transform.InverseTransformVector(_rB.velocity));
        sensor.AddObservation(this.transform.InverseTransformPoint(_closestPointToCheckPoint));
        sensor.AddObservation(this.transform.InverseTransformDirection(this.transform.forward));
        sensor.AddObservation(_wheelDrive.Horizontal);
        sensor.AddObservation(_wheelDrive.Vertical);
    }

    public override void OnActionReceived(ActionBuffers actions)//receive decision from ai and use it on car
    {
        base.OnActionReceived(actions);
        //do change
        _wheelDrive.Horizontal += actions.ContinuousActions.Array[0]* _wheelChangeRate;
        _wheelDrive.Vertical += (actions.ContinuousActions.Array[1]+1)* _wheelChangeRate;
        _wheelDrive.HandBar= actions.DiscreteActions.Array[0]!=0;
        ver = _wheelDrive.Vertical;
        hor = _wheelDrive.Horizontal;
        //calculate reward to valuate ai decision making
        //lower reward by time
        AddReward(_rewardForTime);
        //complete episode and give pos reward if achieve checkpoint
        if (DistanceToCheckPoint()<_distanceToEnd)//if reach checkpoint 
        {
            AddReward(_rewardFOrEnd);
            _reachedCheckPoint = true;
            _reachedChePoints++;
            if (_environment.UsedCars[0] == this)//if is first
            {
                AddReward(_rewardForFirst);
                
            }

            EndEpisode();//end pos
        }
    }

    public override void Heuristic(in ActionBuffers actionsOut)//for manual control - send input data as ai output data
    {
        actionsOut.ContinuousActions.Array[0] = Input.GetAxis(EnumStack.Controls.Horizontal.ToString());
        actionsOut.ContinuousActions.Array[1] = Input.GetAxis(EnumStack.Controls.Vertical.ToString());
        actionsOut.DiscreteActions.Array[0]= Input.GetButton(EnumStack.Controls.HandBar.ToString()) ? 1 : 0;
    }
    public float DistanceToCheckPoint()//as name say
    {
        return Vector3.Distance(_closestPointToCheckPoint, this.transform.position);
    }
}
