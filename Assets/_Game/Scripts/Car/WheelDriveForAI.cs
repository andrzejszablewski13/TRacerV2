﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//wheel drive controller setted to work witch AIVOne
public class WheelDriveForAI : WheelDrive
{
    void Start()
    {
        base.BeforeScriptStart();
    }

    // Update is called once per frame
    void Update()
    {
        this.MovingSystem();
    }
    protected override void MovingSystem()
    {
        _mWheels[0].ConfigureVehicleSubsteps(_criticalSpeed, _stepsBelow, _stepsAbove);
        if (SystemInfo.deviceType != DeviceType.Handheld)//seting control system PC
        {
            this.ControlsSystem();
        }

        base.MovingSystem();
    }
    private float _horizontal, _vertical;
    private bool _handBar=false;
    //access for AIVONe
    public float Horizontal { get => _horizontal; set => _horizontal =Mathf.Clamp( value,-1,1); }
    public float Vertical { get => _vertical; set => _vertical = Mathf.Clamp(value, -1, 1); }
    public bool HandBar { get => _handBar; set => _handBar = value; }
    protected override void ControlsSystem()//players Controls on PC
    {
        _angle = _maxAngle * Horizontal;
        _tempInputLocation = Vertical;
        SlowOrSpeedUp();

        _handBrake = HandBar ? _brakeTorque : 0;
    }

    private readonly int _half = 2, _tenth = 10;

   

    private void SlowOrSpeedUp()//slowing when not speeding
    {
        if (_tempInputLocation > 0)
        {
            _torque = _maxTorque * _tempInputLocation;
        }
        else if (_tempInputLocation == 0)
        {
            _torque = _tempInputLocation;
            _handBrake = _brakeTorque / _tenth;
        }
        else if (_tempInputLocation < 0)
        {
            _handBrake = _brakeTorque / _half;
        }
    }
}
